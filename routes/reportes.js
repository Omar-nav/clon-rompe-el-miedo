const router = require("express").Router();
const Reporte = require("../models/Reporte");

//lista de reportes
router.get("/reportesdata", (req, res) => {
  Reporte.find({})
    // .limit(25)
    .exec((err, reportes) => {
      if (err) {
        return err;
      }
      res.send(reportes);
      // res.render("components/Mapa.vue", { reportes })
    })
});


module.exports = router;
