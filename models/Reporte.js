const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reportesSchema = new Schema(
  {
    Nombre: String,
    "Medio/Colectivo": String,
    Cargo: String,
    Perpetrador: String,
    Estado: String,
    Municipio: String,
    Dirección: String,
    Fecha: String,
    Hora: String,
    "Tipo de agresión": String,
    Agresión: String,
    Hechos: String,
    "Día y hora de verificación": String,
    "Alerta de A19": String,
    "Subido a mapa": Boolean,
    "Montado a postal": Boolean,
    Folio: Number,
    Versión: Number,
    "Número de agresiones a contabilizar": Number,
    Lugar: String,
    Latitud: Number,
    Longitud: Number,
    Marcador: String
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at"
    }
  }
);

module.exports = mongoose.model("Reporte", reportesSchema);
